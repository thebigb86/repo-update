<?php

$config = array(
	'documentRoot' 	=> '/var/www',
	'vhostRoot' 	=> '/var/www/%s',
);


$vhostRoot = sprintf($config['vhostRoot'], $_SERVER['HTTP_HOST']);

if (strpos($vhostRoot, $config['documentRoot']) !== 0)
{
        http_response_code(400);
        echo '<html><head></head><body><p color="#f00">Malformed hostname!</p></body></html>';
        exit;
}

@include_once("$vhostRoot/.repo-update");

if (!isset($pushKey))
{
        http_response_code(500);
        echo '<html><head></head><body><p color="#f00">No .repo-update file or invalid configuration found for host!</p></body></html>';
        exit;
}

if (!isset($_GET['key']) || $_GET['key'] != $pushKey)
{
        http_response_code(400);
        echo '<html><head></head><body><p color="#f00">Invalid push key provided!</p></body></html>';
        exit;
}

$hardReset = isset($hardReset) ? $hardReset : false;

echo '<html><head></head><body><ul>';
if ($hardReset)
{
        echo '<li>';
        echo '<p>Doing hard reset</p>';
        echo '<pre>' . gitCommand('reset --hard') . '</pre>';
        echo '</li>';
}

echo '<li>';
echo '<p>Pulling from &quot;deploy&quot; branch</p>';
echo '<pre>' . gitCommand('pull origin deploy') . '</pre>';
echo '</li>';

echo '<li><b>Done</b></li>';
echo '</ul>';

function gitCommand($command)
{
        global $vhostRoot;
        return `ssh-agent bash -c 'ssh-add /var/www-resources/.ssh/id_rsa; cd $vhostRoot; git $command' 2>&1`;
}
