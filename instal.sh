#!/bin/bash

aliasConf="/etc/apache2/mods-enabled/alias.conf"
hookAlias="Alias /repo-update.php /var/www-resources/repo-update.php"

sudo a2enmod alias
if grep -q $hookAlias $aliasConf; then
	echo $hookAlias | sudo tee -a $aliasConf
fi